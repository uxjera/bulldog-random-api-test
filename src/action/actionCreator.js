export const GET_DATA = "GET_DATA";
export const GET_ALL_DATA = "GET_ALL_DATA";

const API_URL = 'https://dog.ceo/api/breed/bulldog/french/images/random';
//const API_URL = 'https://dog.ceo/api/breed/bulldog/french/images/random';
const API_URL_ALL = 'https://dog.ceo/api/breed/bulldog/french/images';

function handleData(data){
	return{
		type: GET_DATA,
		data
	}
}

function handleAllData(data){
	return{
		type: GET_ALL_DATA,
		data
	}
}

export function getData(){
	return dispatch => {
		return fetch(API_URL)
			.then(res => res.json())
			.then(data => dispatch(handleData(data)))
	}
}

export function getAllData(){
	return dispatch => {
		return fetch(API_URL_ALL)
			.then(res => res.json())
			.then(data => dispatch(handleAllData(data)))
	}
}
