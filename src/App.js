import React, { Component } from 'react';
import './App.css';
import DataTest from './components/DataTest';

class App extends Component {
  render() {
    return (
      <div className="App">
        <DataTest />
      </div>
    );
  }
}

export default App;
