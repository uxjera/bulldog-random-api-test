import { GET_DATA, GET_ALL_DATA } from '../action/actionCreator'

const initialState = {
	ourData: [],
	allData: []
}

export default function rootReducer(state=initialState, action){
	switch(action.type){
		case GET_DATA:
			return {...state, ourData: action.data}
		case GET_ALL_DATA:
			return {...state, allData: action.data}
		default:
			return state
	}
}