import React, {Component} from 'react';
import {connect} from 'react-redux';
import {getData, getAllData} from '../action/actionCreator';
import './DataTest.css';

class DataTest extends Component {
	constructor(props){
		super(props);
		this.getNewDog = this.getNewDog.bind(this);
		this.getRandomDog = this.getRandomDog.bind(this);
		this.state = { randomDog: false}
	}

	componentWillMount(){
		this.props.getData()
		this.props.getAllData()
	};

	getNewDog(e) {
		this.props.getData();
	}

	getRandomDog(){
		return this.setState({randomDog: true})
	}

	render(){
		const doggoList = this.props.allData.message !== undefined ? this.props.allData.message : '';
		const max = doggoList.length;
		var randomDog = Math.floor(Math.random() * max);
		return(
			<div className="page-container">
				<h1>French Bulldog Generator</h1>
				<button type="button" onClick={this.getRandomDog}>I need another a new Doggo</button>
				{/*<p>Data Message: {this.props.ourData.status+' '+this.props.ourData.message}</p>
				<img src={this.props.ourData.message} alt="French Bulldoogie"/>
				<hr />*/}
				<p>French Bulldog #{randomDog}</p>
				<div className="image-container">
				<img src={doggoList[randomDog]} alt="a random doggo" />
				</div>
				
				
			</div>
		)
	}
}

function mapStateToProps(reduxState){
	return{ ourData: reduxState.ourData,
					allData: reduxState.allData }
}

export default connect(mapStateToProps, {getData, getAllData})(DataTest);